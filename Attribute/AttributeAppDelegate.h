//
//  AttributeAppDelegate.h
//  Attribute
//
//  Created by Gabriel Pereira on 05/02/13.
//  Copyright (c) 2013 Gabriel Bernardo Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttributeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
