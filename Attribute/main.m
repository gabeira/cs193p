//
//  main.m
//  Attribute
//
//  Created by Gabriel Pereira on 05/02/13.
//  Copyright (c) 2013 Gabriel Bernardo Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AttributeAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AttributeAppDelegate class]));
    }
}
