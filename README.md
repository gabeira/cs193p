# Projetos Exemplos do Curso IOS de Stanford

#Syllabus for Winter 2013. Details may change as the quarter progresses.

1/8 - Class Logistics, Overview of iOS, MVC, Objective-C
1/10 - Xcode 4

1/15 - Objective-C
1/17 - Foundation, Attributed Strings

1/22 - Views and Gestures
1/24 - View Controller Lifecycle

1/29 - Collection View, Layout, Autorotation
1/31 - Storyboarding, Navigation, Scrolling

2/5 - Table View
2/7 - Blocks, Multithreading, Categories

2/12 - Persistence
2/14 - Final Project Overview

2/19 - Documents and Core Data
2/21 - More Core Data, Page View Controller

2/26 - Modal View Controllers/Text Fields/Timers
2/28 - Alerts/Camera/Photo Library

3/5 - Core Motion
3/7 - Settings/Localization

3/12 - TBD
3/14 - Alternate Final
