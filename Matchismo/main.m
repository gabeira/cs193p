//
//  main.m
//  Matchismo
//
//  Created by Gabriel Pereira on 01/02/13.
//  Copyright (c) 2013 Gabriel Bernardo Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
