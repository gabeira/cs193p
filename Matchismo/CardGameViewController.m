//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Gabriel Pereira on 01/02/13.
//  Copyright (c) 2013 Gabriel Bernardo Pereira. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"
#import "GameResult.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong,nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *combineSegmentedControl;
@property (weak, nonatomic) IBOutlet UISlider *historySlider;
@property (weak, nonatomic) IBOutlet UILabel *historyStatusLabel;
@property (strong, nonatomic) GameResult *gameResult;
@end

@implementation CardGameViewController

- (GameResult *)gameResult
{
    if (!_gameResult) _gameResult = [[GameResult alloc] init];
    return _gameResult;
}

//Assignment 1
- (IBAction)combine:(UISegmentedControl *)sender {
    self.game.gameMode = [sender selectedSegmentIndex];
}

//Assignment 1
- (IBAction)showMoveHistory:(UISlider *)sender {
    [self.historySlider setValue: roundf(sender.value) animated:YES];
    if (self.game.lastFlipLabelArray.count > 0){
        self.resultLabel.text = [self.game.lastFlipLabelArray objectAtIndex:(roundf(sender.value))];
    }
    self.historyStatusLabel.text = [NSString stringWithFormat:@"Show movement %d of the %d previously played.", ((int)roundf(sender.value))+1, (int)self.game.lastFlipLabelArray.count];
}



- (CardMatchingGame *)game
{
    if (!_game){
        [self deal];
    }
    return _game;
}

//Assignment 1
- (void)deal
{
    _game = [[CardMatchingGame alloc] initWithCardCount:self.cardButtons.count
                                              usingDeck:[[PlayingCardDeck alloc] init]];
    self.combineSegmentedControl.enabled = TRUE;
    NSLog(@"plays deal %d",self.game.lastFlipLabelArray.count);
    self.historySlider.value = 0;
    self.historySlider.maximumValue = 0;
    self.historyStatusLabel.text = [NSString stringWithFormat:@"Show movement %d of the %d previously played.", 0, 0];
    
    self.gameResult = nil;
    
}
    

- (IBAction)redistribuirCartas:(UIButton *)sender {

    [self deal];
    self.flipCount = 0;
    self.resultLabel.text = @"New Cards Dealed";
    [self updateUI];
}

- (void)setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    [self updateUI];
    NSLog(@"%@",@"setCardbutton");
}

- (void)updateUI
{
    for (UIButton *cardButton in self.cardButtons){
        Card *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        [cardButton setTitle:card.contents forState:UIControlStateSelected];
        [cardButton setTitle:card.contents forState:UIControlStateSelected|UIControlStateDisabled];
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.isUnplayable;
        cardButton.alpha = card.isUnplayable ? 0.3 : 1.0;
        
        //Assignment 1
        //Set Card Back Image
        [cardButton setImage:[UIImage imageNamed:@"card1.png"] forState:UIControlStateNormal];
        cardButton.contentEdgeInsets = UIEdgeInsetsMake(5, 4, 5, 5);
        [cardButton setImage:[[UIImage alloc] init] forState:UIControlStateSelected];
        [cardButton setImage:[[UIImage alloc] init] forState:UIControlStateSelected|UIControlStateDisabled];
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
}

- (void)setFlipCount:(int)flipCount
{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (IBAction)flipCard:(UIButton *)sender
{
    [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    self.flipCount++;
    [self updateUI];
    self.resultLabel.text = [self.game.lastFlipLabelArray lastObject];
    self.combineSegmentedControl.enabled = NO;
    if (self.game.lastFlipLabelArray.count){
        self.historySlider.maximumValue = self.game.lastFlipLabelArray.count-1;
        self.historySlider.value = self.game.lastFlipLabelArray.count-1;
        self.historyStatusLabel.text = [NSString stringWithFormat:@"Show movement %d of the %d previously played.", (int)self.game.lastFlipLabelArray.count, (int)self.game.lastFlipLabelArray.count];
    }
    self.gameResult.score = self.game.score;
}



- (void)viewDidUnload {
    [self setScoreLabel:nil];
    [self setResultLabel:nil];
    [self setCombineSegmentedControl:nil];
    [self setHistorySlider:nil];
    [self setHistoryStatusLabel:nil];
    [super viewDidUnload];
}
@end
