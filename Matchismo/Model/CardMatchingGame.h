//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Gabriel Pereira on 02/02/13.
//  Copyright (c) 2013 Gabriel Bernardo Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

//designated initializer
- (id)initWithCardCount:(NSUInteger)cardCount
             usingDeck:(Deck *)deck;

- (void)flipCardAtIndex:(NSUInteger)index;

- (Card *)cardAtIndex:(NSUInteger)index;

@property (nonatomic,readonly) int score;

//Assignment 1
@property (nonatomic) int gameMode;
@property (nonatomic) NSMutableArray *lastFlipLabelArray;

@end
