//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Gabriel Pereira on 02/02/13.
//  Copyright (c) 2013 Gabriel Bernardo Pereira. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame ()
@property (strong, nonatomic) NSMutableArray *cards; //of Card
@property (readwrite, nonatomic) int score;
@end

@implementation CardMatchingGame

- (NSMutableArray *)lastFlipLabelArray
{
    if (!_lastFlipLabelArray) _lastFlipLabelArray = [[NSMutableArray alloc] init];
    return _lastFlipLabelArray;
}

- (NSMutableArray *)cards
{
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (id)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck
{
    self = [super init];
    if (self){
        for (int i=0; i < count; i++){
            Card *card = [deck drawRandomCard];
            if (card){
                self.cards[i] = card;
            }else{
                self = nil;
                break;
            }
        }
    }
    return self;
}

- (Card *)cardAtIndex:(NSUInteger)index
{
    return (index < self.cards.count) ? [self.cards objectAtIndex:index] : nil;
}

#define FLIP_COST 1
#define MISMATCH_PENALTY 2
#define MATCH_BONUS 4

- (void)flipCardAtIndex:(NSUInteger)index
{
    Card *card = [self cardAtIndex:index];
    //Game mode combine 2 cards
    if (self.gameMode == 0){
        if (card && !card.isUnplayable) {
            BOOL primeiraCarta = TRUE;
            if (!card.isFaceUp) {
                for (Card *otherCard in self.cards){
                    if (otherCard.isFaceUp && !otherCard.isUnplayable){
                        int matchScore = [card match:@[otherCard]];
                        if (matchScore){
                            otherCard.unplayable = YES;
                            card.unplayable = YES;
                            self.score += matchScore * MATCH_BONUS;
                            [self.lastFlipLabelArray addObject:[NSString  stringWithFormat:@"%@ & %@ match +%d",otherCard.contents,card.contents,MATCH_BONUS]];
                            primeiraCarta = FALSE;
                        }else{
                            otherCard.faceUp = NO;
                            self.score -= MISMATCH_PENALTY;
                            [self.lastFlipLabelArray addObject:[NSString stringWithFormat:@"%@ & %@ unmatch -%d",otherCard.contents,card.contents,MISMATCH_PENALTY]];
                            primeiraCarta = FALSE;
                        }
                        break;
                    }
                }
                self.score -= FLIP_COST;
                if (primeiraCarta){
                    [self.lastFlipLabelArray addObject:[NSString stringWithFormat:@"%@ fliped",card.contents]];
                }
                int numeroCartasRestantes = 0;
                for (Card *otherCard in self.cards){
                    if (!otherCard.isUnplayable){
                        numeroCartasRestantes ++;
                    }
                }
                if (numeroCartasRestantes<2) {
                    [self.lastFlipLabelArray addObject:@" Congratulations "];
                }
            }
            card.faceUp = !card.isFaceUp;
        }
    }else{
        //Game mode combine 3 cards
        if (card && !card.isUnplayable) {
            if (!card.isFaceUp) {
                NSMutableArray *playedCards = [[NSMutableArray alloc ]init];
                
                for (Card *otherCard in self.cards){
                    if (otherCard.isFaceUp && !otherCard.isUnplayable){
                        [playedCards addObject:otherCard];
                    }
                }
                
                if ([playedCards count] == 0){
                }else if ([playedCards count] == 1){ //Second card played dont match, just 2 cards
                    int matchScore = [card match:playedCards];
                    if (!matchScore){
                        for (Card *otherCard in playedCards){
                            otherCard.faceUp = NO;
                        }
                        self.score -= MISMATCH_PENALTY;
                        [self.lastFlipLabelArray addObject:[NSString  stringWithFormat:@"%@ & %@ unmatch -%d",[playedCards[0] contents],card.contents,MISMATCH_PENALTY]];
                    }
                }else if ([playedCards count] == 2){
                    int matchScore = [card match:playedCards];
                    if (matchScore){
                        
                        card.unplayable = YES;
                        for (Card *otherCard in playedCards){
                            otherCard.unplayable = YES;
                        }
                        self.score += matchScore * MATCH_BONUS;
                        [self.lastFlipLabelArray addObject:[NSString  stringWithFormat:@"%@ & %@ & %@ match +%d",[playedCards[0] contents],[playedCards[1] contents],card.contents,MATCH_BONUS]];
                        
                    }else{
                        
                        for (Card *otherCard in playedCards){
                            otherCard.faceUp = NO;
                        }
                        self.score -= MISMATCH_PENALTY;
                        [self.lastFlipLabelArray addObject:[NSString stringWithFormat:@"%@ & %@ & %@ unmatch -%d",[playedCards[0] contents],[playedCards[1] contents],card.contents,MISMATCH_PENALTY]];
                        
                    }

                }
                self.score -= FLIP_COST;
            }
            card.faceUp = !card.isFaceUp;
            
        }
    }
    
    
}

@end
