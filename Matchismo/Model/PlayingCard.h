#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit; //Diamond, Heart, clubs, spades.
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;

@end